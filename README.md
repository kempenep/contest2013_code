Description of contest.sh
-------------------------
open source code to solve the IGARSS 2013 data fusion contest

License and distribution
------------------------
This bash script is written by Pieter Kempeneers and is distributed under the GNU public license GPLv3. Refer to the file COPYING for copying conditions. The script is based on the open source tools pktools (pktool.nongnu.org).

Installation and running the script
-----------------------------------
refer to the file INSTALL

Change history
-------------
see Changelog
