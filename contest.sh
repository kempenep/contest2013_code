########################################################################################
# contest_feature_fusion.sh bash script for solving the IGARSS 2013 data fusion contest
# Copyright (C) 2013 Pieter Kempeneers
#
# This file is an example of how open source can be applied for data fusion
# The tools used in this script are based on 
# pktools (pktools.nongnu.org) and Orfeo Toolbox (OTB, www.orfeo-toolbox.org)
#
# pktools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# OTB is distributed under a free software license CeCILL (similar to GPL)
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this script.  If not, see <http://www.gnu.org/licenses/>.
########################################################################################

#!/bin/bash
########################### todo: adapt these directories ##############################
INPUTDIR=. # put labels, training text file, ct.txt, beta_10.txt, Hyperspectral and LiDAR image here
OUTPUTDIR=${INPUTDIR}/output
TRAININGDIR=${INPUTDIR}/training
VALIDATIONDIR=${INPUTDIR}/validation
TMPDIR=${INPUTDIR}/tmp$$

########################################################################################

#################################### input data ########################################
INPUTIMG_CASI=2013_IEEE_GRSS_DF_Contest_CASI.tif #Hyperspectral image
INPUTIMG_LIDAR=2013_IEEE_GRSS_DF_Contest_LiDAR.tif #LiDAR height image
ROIASCII=2013_IEEE_GRSS_DF_Contest_Samples_TR.txt #Training data
#BETA=beta_10.txt #beta parameters for Markov Random Field
LAS=UH_PointCloud_Dec.las #not needed for now
VALIDATIONZIP=validation.tgz
VALIDATIONFILE=2013_IEEE_GRSS_DF_Contest_Samples_VA.shp
########################################################################################

################################### output data ########################################
INPUTIMG_QL=2013_IEEE_GRSS_DF_Contest_CASI_ql.tif #Quick look of hyperspectral image
TESTMAP_FEATURE_FUSION=2013_IEEE_GRSS_DF_Contest_FEATURE_FUSION_MAP.tif
TESTMAP_FEATURE_FUSION_MRF=2013_IEEE_GRSS_DF_Contest_FEATURE_FUSION_MRF.tif
LIDAR_SHADOW=lidar_shadow.tif #not used for now
########################################################################################

############################### intermediate data ######################################
ROIOGR=2013_IEEE_GRSS_DF_Contest_Samples_TR.shp
ROIOGR_26915=2013_IEEE_GRSS_DF_Contest_Samples_TR_26915.shp
INPUTIMG_CASI_FS16=2013_IEEE_GRSS_DF_Contest_CASI_FS16.tif
INPUTIMG_FEATURE_FUSION=2013_IEEE_GRSS_DF_Contest_FEATURE_FUSION.tif
TRAINING_FEATURE_FUSION=training_feature_fusion.shp
TESTPROB_FEATURE_FUSION_MRF=2013_IEEE_GRSS_DF_Contest_FEATURE_FUSION_PROB.tif
########################################################################################

########### variables to steer process (can be set via command line as well) ###########
CREATE_QL=0
CREATE_SHADOW=0
FEATURE_EXTRACTION=0
LIDAR=0
CREATE_TRAINING=0
CLASSIFICATION=0
VALIDATION=0
MRF=0
########################################################################################

######################### parameters for algorithms ####################################
COST=1000 #cost parameter for SVM
GAMMA=0.1 #gamma parameter for SVM
########################################################################################

function usage
{
    echo "usage: $0 [--create-ql] [--feature-extraction] [--lidar] [--create-training] [--classification] [--mrf] [--validation] [-h]"
}

while [ "$1" != "" ]; do
    case $1 in
        --create-ql )             CREATE_QL=1
	    echo "create quick look image from Hyperspectral image"
                                ;;
        --feature-extraction )    FEATURE_EXTRACTION=1
	    echo "create input fusion image"
                                ;;
        --lidar )                 LIDAR=1
	    echo "lidar"
                                ;;
        -t | --create-training )  CREATE_TRAINING=1
	    echo "create training and test sample"
                                ;;
        -c | --classification )   CLASSIFICATION=1
	    echo "classify image"
                                ;;
        -mrf | --mrf )            MRF=1
	    echo "classify image with Markov Random Field"
                                ;;
        -v | --validation )         VALIDATION=1
	    echo "validation"
                                ;;
        --create-shadow )         CREATE_SHADOW=1
	    echo "create shadow"
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

############################### create directories #####################################
if [ ! -d ${OUTPUTDIR} ];then
    mkdir ${OUTPUTDIR};
fi
if [ ! -d ${TRAININGDIR} ];then
    mkdir ${TRAININGDIR};
fi
if [ -d ${TMPDIR} ];then
    echo "Warning: ${TMPDIR} exists, please remove dir first, or choose different directory"
else
    mkdir ${TMPDIR}
fi
########################################################################################

if [ "${CREATE_QL}" -eq 1 ];then
    echo "create quick look image"
    pkcrop -i ${INPUTDIR}/${INPUTIMG_CASI} -o ${INPUTDIR}/${INPUTIMG_QL} -b 60 -b 40 -b 20 -co INTERLEAVE=BAND -co COMPRESS=LZW -co TILED=YES -p epsg:26915
fi

if [ "${FEATURE_EXTRACTION}" -eq 1 ];then
    echo "select casi bands"
    pkcrop -i ${INPUTDIR}/${INPUTIMG_CASI} -o ${INPUTDIR}/${INPUTIMG_CASI_FS16}  -b 16 -b 136 -b 26 -b 10 -b 25 -b 17 -b 91 -b 22 -b 14 -b 18 -b 37 -b 84 -b 124 -b 51 -b 11 -b 23 -co INTERLEAVE=BAND -co COMPRESS=LZW -co TILED=YES -p epsg:26915
    echo "feature fusion"
    if [ "${LIDAR}" -eq 1 ];then
	echo "create new fusion image, concatenating LiDAR and selected CASI features"
	pkcrop \
	-i ${INPUTDIR}/${INPUTIMG_LIDAR} \
	-i ${INPUTDIR}/${INPUTIMG_CASI_FS16} \
	-ot UInt16 -o ${INPUTDIR}/${INPUTIMG_FEATURE_FUSION} -co INTERLEAVE=BAND -co COMPRESS=LZW -co TILED=YES -p epsg:26915
    else
	echo "fusion image contains only selected CASI features"
	pkcrop \
	-i ${INPUTDIR}/${INPUTIMG_CASI_FS16} \
	-ot UInt16 -o ${INPUTDIR}/${INPUTIMG_FEATURE_FUSION} -co INTERLEAVE=BAND -co COMPRESS=LZW -co TILED=YES -p epsg:26915
    fi
fi

if [ "${CREATE_TRAINING}" -eq 1 ];then
    echo "create training sample as shape file from ROI ASCII file"
    C=1
    cat ${INPUTDIR}/labels.txt |while read class label;do 
	sed -n "/$label/,/^$/p" ${INPUTDIR}/${ROIASCII}|sed -e '/;/d' -e '/^$/d'|awk -v C=$C '{print $1,$4,$5,C}';C=$(($C+1))
    done > ${TRAININGDIR}/training.txt
    echo "create shp file from ASCII training file"
    rm -f ${TRAININGDIR}/${ROIOGR}
    pkascii2ogr -i ${TRAININGDIR}/training.txt -o ${TRAININGDIR}/${ROIOGR} -x 2 -y 1 -n id -ot Integer -n label -ot Integer -p epsg:4326
    rm -f ${TRAININGDIR}/${ROIOGR_26915}
    ogr2ogr -s_srs epsg:4326 -t_srs epsg:26915 ${TRAININGDIR}/${ROIOGR_26915} ${TRAININGDIR}/${ROIOGR}

    rm -f ${TRAININGDIR}/${TRAINING_FEATURE_FUSION}
    echo "extract image data from training sample"
    pkextract -i ${INPUTDIR}/${INPUTIMG_FEATURE_FUSION} -s ${TRAININGDIR}/${ROIOGR_26915} -o ${TRAININGDIR}/${TRAINING_FEATURE_FUSION}
    echo "training sample: "
    ogrinfo ${TRAININGDIR}/${TRAINING_FEATURE_FUSION} $(basename ${TRAINING_FEATURE_FUSION} .shp) |grep Count |awk -v FS=":" '{print $2}'
    cat ${INPUTDIR}/labels.txt |while read class label;do 
	NTRAINING=$(ogrinfo -where "label=$class" ${TRAININGDIR}/${TRAINING_FEATURE_FUSION} $(basename ${TRAINING_FEATURE_FUSION} .shp)|grep Count|awk -v FS=":" '{print $2}')
	echo "training sample size class $label ($class): $NTRAINING"
    done
fi

if [ "${CLASSIFICATION}" -eq 1 ];then
    echo "classify with bootstrap aggregation using 33 percent of training set for each of the three classifiers used"
    pkclassify_svm -t ${TRAININGDIR}/${TRAINING_FEATURE_FUSION} -bs 33 -bag 3 -cv 2 -i ${INPUTDIR}/${INPUTIMG_FEATURE_FUSION} -o ${TMPDIR}/${TESTMAP_FEATURE_FUSION} -ct ${INPUTDIR}/ct.txt -cc ${COST} -g ${GAMMA} -pe -p 1 -p 1 -p 1 -p 1 -p 1 -p 1 -p 1 -p 1 -p 1 -p 1 -p 1 -p 0.2 -p 1 -p 1 -p 1
    echo "remove border by setmask map with input image -t 0 -f 0"
    pksetmask -i ${TMPDIR}/${TESTMAP_FEATURE_FUSION} -m ${INPUTDIR}/${INPUTIMG_CASI} -t 0 -f 0 -o ${OUTPUTDIR}/${TESTMAP_FEATURE_FUSION}
    if [ "${MRF}" -eq 1 ];then
	pkfilter -f mrf -dx 3 -dy 3 -i ${OUTPUTDIR}/${TESTMAP_FEATURE_FUSION} -o ${OUTPUTDIR}/${TESTPROB_FEATURE_FUSION_MRF} -ot Float32 -class 1 -class 10 -class 11 -class 12 -class 13 -class 14 -class 15 -class 2 -class 3 -class 4 -class 5 -class 6 -class 7 -class 8 -class 9 -ct none -v 1
	pkclassify_svm -t ${TRAININGDIR}/${TRAINING_FEATURE_FUSION} -bs 33 -bag 3 -cv 2 -i ${INPUTDIR}/${INPUTIMG_FEATURE_FUSION} -o ${TMPDIR}/${TESTMAP_FEATURE_FUSION_MRF} -ct ${INPUTDIR}/ct.txt -cc ${COST} -g ${GAMMA} -pe -pim ${OUTPUTDIR}/${TESTPROB_FEATURE_FUSION_MRF}
    echo "remove border by setmask map with input image -t 0 -f 0"
    pksetmask -i ${TMPDIR}/${TESTMAP_FEATURE_FUSION_MRF} -m ${INPUTDIR}/${INPUTIMG_CASI} -t 0 -f 0 -o ${OUTPUTDIR}/${TESTMAP_FEATURE_FUSION_MRF}
    fi
fi

if [ "${VALIDATION}" -eq 1 ];then
    VALIDATIONFILE_POINT=2013_IEEE_GRSS_DF_Contest_Samples_VA_point.shp
    mkdir -p ${VALIDATIONDIR}
    tar xzvf ${VALIDATIONZIP} -C ${VALIDATIONDIR}
    echo "using validation file ${VALIDATIONDIR}/${VALIDATIONFILE}"
    if [ ! -f ${OUTPUTDIR}/${TESTMAP_FEATURE_FUSION} ];then
	echo "map not found: run classification first using option --classification"
	exit
    else
	rm -f ${TMPDIR}/${VALIDATIONFILE_POINT}
	pkextract -i ${OUTPUTDIR}/${TESTMAP_FEATURE_FUSION} -s ${VALIDATIONDIR}/${VALIDATIONFILE} -o ${TMPDIR}/${VALIDATIONFILE_POINT}
	echo "validating classification map"
	pkdiff -i ${OUTPUTDIR}/${TESTMAP_FEATURE_FUSION} -r ${TMPDIR}/${VALIDATIONFILE_POINT} -cm |tail -2
	echo "validating Markov random field map"
	if [ ! -f ${OUTPUTDIR}/${TESTMAP_FEATURE_FUSION_MRF} ];then
	    echo "map not found: run classification first using option --classification --mrf"
	else
	    pkdiff -i ${OUTPUTDIR}/${TESTMAP_FEATURE_FUSION_MRF} -r ${TMPDIR}/${VALIDATIONFILE_POINT} -cm
	fi
    fi
fi

####################################### optional #######################################
if [ "${CREATE_SHADOW}" -eq 1 ];then
    DSM=${INPUTDIR}/${INPUTIMG_LIDAR}
    echo "create SHADOW image"
    echo "get sun angles from http://www.esrl.noaa.gov/gmd/grad/solcalc/"
    echo "lal acquisition time: 12:38 pm, time zone -5, lat 29.75, lon -95.33"
    echo "saa=112, sza=90-78=12"
    pkdsm2shadow -i ${INPUTDIR}/${DSM} -i ${INPUTDIR}/${DSM} -o ${INPUTDIR}/${LIDAR_SHADOW} -f 16 -ot Byte -ct ${INPUTDIR}/ct.txt -sza 78 -saa 119
    pkcrop -ot UInt16 -as 0 -as 1000 -i ${TMPDIR}/${LIDAR_SHADOW} -o ${INPUTDIR}/${LIDAR_SHADOW} -co INTERLEAVE=BAND -co COMPRESS=LZW -co TILED=YES
fi
########################################################################################

echo "clean up tmp directory ${TMPDIR}"
rm -rf $TMPDIR
